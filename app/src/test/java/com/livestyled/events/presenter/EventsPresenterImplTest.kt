package com.livestyled.events.presenter

import com.livestyled.base.error.AppException
import com.livestyled.events.EventsContract
import com.livestyled.events.business.EventsBusiness
import com.livestyled.events.view.dto.EventUIDTO
import io.reactivex.Observable
import io.reactivex.schedulers.Schedulers
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations


/**
 * Created by Eslam Hussein on 7/14/18.
 */
class EventsPresenterImplTest {

    @Mock
    private lateinit var view: EventsContract.EventsView

    @Mock
    private lateinit var eventsBusiness: EventsBusiness

    private lateinit var presenter: EventsContract.EventsPresenter


    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)
        presenter = EventsPresenterImpl(eventsBusiness, Schedulers.trampoline(), Schedulers.trampoline())
        presenter.onAttach(view)

    }


    @Test
    fun getEvents() {

        val result: EventUIDTO = getFakeResponse()

        // Given initialized presenter with attached view
        Mockito.`when`(eventsBusiness.getEvents()).thenReturn(Observable.just(result))

        // When
        presenter.getEvents()

        // Then
        Mockito.verify(view).showLoading()
        Mockito.verify(eventsBusiness).getEvents()
        Mockito.verify(view).showEventsSuccess(result, result.genreClassification.first().name, false)
        Mockito.verify(view).hideLoading()

    }


    @Test
    fun getEmptyEvents() {

        Mockito.`when`(eventsBusiness.getEvents()).thenReturn(Observable.error(AppException(AppException.NO_DATA_ERROR,
                "no result found")))

        // When
        presenter.getEvents()

        // Then
        Mockito.verify(view).showLoading()
        Mockito.verify(eventsBusiness).getEvents()
        Mockito.verify(view).showError("no result found")
        Mockito.verify(view).hideLoading()

    }


    @Test
    fun testNoInternet() {

        Mockito.`when`(eventsBusiness.getEvents()).thenReturn(Observable.error(AppException(AppException.NETWORK_ERROR,
                "Please check your internet connection and try again.")))

        // When
        presenter.getEvents()

        // Then
        Mockito.verify(view).showLoading()
        Mockito.verify(eventsBusiness).getEvents()
        Mockito.verify(view).showError("Please check your internet connection and try again.")
        Mockito.verify(view).hideLoading()

    }

    private fun getFakeResponse(): EventUIDTO = EventUIDTO(listOf(), setOf())


}