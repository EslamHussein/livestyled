package com.livestyled.events.repo

import com.livestyled.base.cloud.AppServices
import com.livestyled.base.error.AppException
import io.reactivex.Observable
import io.reactivex.android.plugins.RxAndroidPlugins
import io.reactivex.plugins.RxJavaPlugins
import io.reactivex.schedulers.Schedulers
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations
import org.mockito.junit.MockitoJUnitRunner


/**
 * Created by Eslam Hussein on 7/14/18.
 */
@RunWith(MockitoJUnitRunner::class)
class EventsCloudRepoImplTest {


    private lateinit var cloudRepo: EventsCloudRepo

    @Mock
    private lateinit var service: AppServices

    @Mock
    private lateinit var event: Event


    @Before
    @Throws(Exception::class)
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        RxAndroidPlugins.setInitMainThreadSchedulerHandler { Schedulers.trampoline() }
        RxJavaPlugins.setIoSchedulerHandler { Schedulers.trampoline() }

        cloudRepo = EventsCloudRepoImpl(service)

    }


    @Test
    fun testGetEventsSuccessful() {

        val result = EventsResponse(Embedded(arrayListOf(event)))

        Mockito.`when`(service.getEvents()).thenReturn(Observable.just(result))

        val testObserver = cloudRepo.getEvents().test()

        testObserver.assertResult(result)

    }


    @Test
    @Throws(AppException::class)
    fun testGetEventsEmpty() {

        val result = EventsResponse(Embedded(arrayListOf()))

        Mockito.`when`(service.getEvents()).thenReturn(Observable.just(result))
        val testObserver = cloudRepo.getEvents().test()

        testObserver.assertErrorMessage("no result found")


    }


}