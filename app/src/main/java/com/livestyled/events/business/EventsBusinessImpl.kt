package com.livestyled.events.business

import com.livestyled.R
import com.livestyled.events.repo.Classification
import com.livestyled.events.repo.EventsCloudRepo
import com.livestyled.events.view.dto.EventListItem
import com.livestyled.events.view.dto.EventUIDTO
import com.livestyled.events.view.dto.HeaderListItem
import com.livestyled.events.view.dto.ListItem
import com.livestyled.util.TextUtils
import io.reactivex.Observable
import javax.inject.Inject

/**
 * Created by Eslam Hussein on 7/10/18.
 */


interface EventsBusiness {
    fun getEvents(): Observable<EventUIDTO>

}

class EventsBusinessImpl @Inject constructor(var cloudRepo: EventsCloudRepo) : EventsBusiness {
    override fun getEvents(): Observable<EventUIDTO> = cloudRepo.getEvents().flatMap {


        val allEventClassification = Classification("-1", TextUtils.getString(resId = R.string.all))


        val classifications = mutableSetOf(allEventClassification)

        classifications.addAll(it.embedded.events.map {
            it.classifications
        }.flatten().map {
            it.genre
        }.toMutableSet())


        val headers = listOf(HeaderListItem(TextUtils.getString(resId = R.string.Favourites)),
                HeaderListItem(TextUtils.getString(resId = R.string.events)))


        val result: ArrayList<ListItem> = arrayListOf()

        result.addAll(headers)
        result.addAll(it.embedded.events.map {
            EventListItem(it)
        })

        Observable.just(EventUIDTO(result, classifications))
    }


}