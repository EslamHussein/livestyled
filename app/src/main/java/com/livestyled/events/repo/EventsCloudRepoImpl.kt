package com.livestyled.events.repo

import com.livestyled.R
import com.livestyled.base.cloud.AppServices
import com.livestyled.base.error.AppException
import com.livestyled.base.error.ErrorManager
import com.livestyled.util.TextUtils
import io.reactivex.Observable
import javax.inject.Inject

/**
 * Created by Eslam Hussein on 7/7/18.
 */

interface EventsCloudRepo {

    fun getEvents(): Observable<EventsResponse>
}

class EventsCloudRepoImpl @Inject constructor(private val service: AppServices) : EventsCloudRepo {
    override fun getEvents(): Observable<EventsResponse> {

        return ErrorManager.wrap(service.getEvents())
                .flatMap {
                    if (it.embedded.events.isEmpty() || it.embedded.events.size < 0)
                        Observable.error(AppException(AppException.NO_DATA_ERROR,
                                "no result found"))
                    else
                        Observable.just(it)
                }

    }
}
