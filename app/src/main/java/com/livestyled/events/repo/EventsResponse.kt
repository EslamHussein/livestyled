package com.livestyled.events.repo

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

/**
 * Created by Eslam Hussein on 7/7/18.
 */

data class EventsResponse(@SerializedName("_embedded") val embedded: Embedded)

@Parcelize
data class Embedded(val events: ArrayList<Event>) : Parcelable

@Parcelize
data class Event(val name: String, val id: String, val images: List<Image>?, val dates: Dates,
                 @SerializedName("_embedded") val embedded: EmbeddedEvent,
                 val classifications: List<Classifications>, var isFavourite: Boolean = false) : Parcelable {


    fun isClassifiedBy(by: String): Boolean {

        return classifications.any {
            it.genre.name == by
        }
    }
}


@Parcelize
data class EmbeddedEvent(var venues: List<Venue>) : Parcelable

@Parcelize
data class Venue(var name: String) : Parcelable

@Parcelize
data class Classifications(val genre: Classification) : Parcelable

@Parcelize
data class Classification(var id: String, var name: String) : Parcelable

@Parcelize
data class Dates(val start: Start) : Parcelable

@Parcelize
data class Start(val localDate: String, val localTime: String, val dateTime: String) : Parcelable

@Parcelize
data class Image(val url: String) : Parcelable

data class Links(val next: Link)

data class Link(val href: String)

data class Page(val size: Int, val totalElements: Int, val totalPages: Int, val number: Int)



