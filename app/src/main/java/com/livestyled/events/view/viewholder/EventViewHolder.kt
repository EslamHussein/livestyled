package com.livestyled.events.view.viewholder

import android.support.v7.widget.RecyclerView
import android.view.View
import kotlinx.android.synthetic.main.event_item_view.view.*

class EventViewHolder(view: View) : RecyclerView.ViewHolder(view) {
    val eventImageView = view.eventImageView!!
    val eventNameTextView = view.eventNameTextView!!
    val venueNameTextView = view.venueNameTextView!!
    val dateTextView = view.dateTextView!!
    val favouriteImageView = view.favouriteImageView!!

}

