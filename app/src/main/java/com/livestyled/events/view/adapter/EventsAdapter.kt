package com.livestyled.events.view.adapter

import android.support.v7.util.DiffUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import com.livestyled.R
import com.livestyled.events.view.EventDiffCallback
import com.livestyled.events.view.dto.EventListItem
import com.livestyled.events.view.dto.HeaderListItem
import com.livestyled.events.view.dto.ListItem
import com.livestyled.events.view.viewholder.EventViewHolder
import com.livestyled.events.view.viewholder.HeaderViewHolder
import com.livestyled.util.TextUtils
import com.squareup.picasso.Picasso


/**
 * Created by Eslam Hussein on 6/15/18.
 */

class EventsAdapter(private var items: ArrayList<ListItem> = arrayListOf(), var filterBy: String,
                    private val onFavouriteClickListener: OnFavouriteClickListener<EventListItem>?)
    : RecyclerView.Adapter<RecyclerView.ViewHolder>(), Filterable {

    private var filteredItem: ArrayList<ListItem>

    init {
        filteredItem = items
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {


        val inflater = LayoutInflater.from(parent.context)
        return when (viewType) {

            ListItem.HEADER_VIEW -> {
                HeaderViewHolder(inflater.inflate(R.layout.event_header_item_view, parent, false))
            }
            ListItem.EVENT_VIEW -> {

                EventViewHolder(inflater.inflate(R.layout.event_item_view, parent, false))
            }

            else -> {
                // TODO create custom item for this
                HeaderViewHolder(inflater.inflate(R.layout.event_header_item_view, parent, false))

            }
        }

    }

    override fun getItemCount(): Int = filteredItem.size


    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {

        val item = filteredItem[position]

        when (holder.itemViewType) {

            ListItem.HEADER_VIEW -> {
                handleHeaderViewHolder(holder as HeaderViewHolder, item as?HeaderListItem)
            }
            ListItem.EVENT_VIEW -> {
                handleEventViewHolder(holder as EventViewHolder, position, item as? EventListItem)
            }
        }

    }


    private fun handleEventViewHolder(eventViewHolder: EventViewHolder, position: Int, item: EventListItem?) {

        eventViewHolder.eventNameTextView.text = item?.event?.name
        eventViewHolder.venueNameTextView.text = item?.event?.embedded?.venues?.joinToString(separator = ",") { it.name }
        eventViewHolder.dateTextView.text = item?.event?.dates?.start?.dateTime

        val isFavResource = if (item?.event?.isFavourite!!) {
            R.drawable.ic_favorite_red_600_36dp
        } else {
            R.drawable.ic_favorite_border_red_600_36dp
        }

        eventViewHolder.favouriteImageView.setImageResource(isFavResource)


        item.event.images.let {
            Picasso.get().load(it?.get(0)?.url)
                    .placeholder(R.drawable.default_image_thumbnail).into(eventViewHolder.eventImageView)

        }
        eventViewHolder.favouriteImageView.setOnClickListener {
            onFavouriteClickListener?.onClick(item, position)
        }


    }

    private fun handleHeaderViewHolder(headerViewHolder: HeaderViewHolder, item: HeaderListItem?) {

        headerViewHolder.headerTitleTextView.text = item?.headerName

    }


    override fun getItemViewType(position: Int): Int {

        val temp = filteredItem[position]

        return when (temp) {
            is EventListItem -> ListItem.EVENT_VIEW
            is HeaderListItem -> ListItem.HEADER_VIEW
            else -> ListItem.UNKNOWN_VIEW
        }

    }


    fun addItems(newEvents: List<ListItem>) {
        val diffCallback = EventDiffCallback(newEvents, this.filteredItem)
        val diffResult = DiffUtil.calculateDiff(diffCallback)
        this.items.clear()
        this.items.addAll(newEvents)
        diffResult.dispatchUpdatesTo(this)

    }

    fun updateItem(listItem: EventListItem) {

        removeItem(listItem)
        listItem.event.isFavourite = !listItem.event.isFavourite
        if (listItem.event.isFavourite) {
            items.add(1, listItem)

        } else {
            items.add(items.size, listItem)

        }

        filter.filter(filterBy)

    }


    private fun removeItem(listItem: EventListItem) {
        filteredItem.remove(listItem)
        items.remove(listItem)

    }


    override fun getFilter(): Filter {

        return object : Filter() {
            override fun performFiltering(by: CharSequence?): FilterResults {


                filterBy = by.toString()
                val eventsListFiltered = if (filterBy == TextUtils.getString(resId = R.string.all) || filterBy.isEmpty()) {
                    items
                } else {
                    items.filter {
                        it is HeaderListItem || (it as EventListItem).event.isClassifiedBy(by.toString())
                    } as ArrayList<ListItem>
                }

                val filterResults = FilterResults()
                filterResults.values = eventsListFiltered
                return filterResults


            }

            override fun publishResults(p0: CharSequence?, resultFilter: FilterResults?) {

                val resultData = resultFilter?.values
                if (resultData is ArrayList<*>) {
                    filteredItem = resultData.filterIsInstance<ListItem>() as ArrayList<ListItem>
                    notifyDataSetChanged()
                }


            }
        }

    }

    fun getItems(): ArrayList<ListItem> = items

    interface OnFavouriteClickListener<T> {
        fun onClick(event: T, position: Int)
    }

}