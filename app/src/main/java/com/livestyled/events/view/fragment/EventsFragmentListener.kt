package com.livestyled.events.view.fragment

import com.livestyled.events.repo.Classification

/**
 * Created by Eslam Hussein on 7/13/18.
 */
interface EventsFragmentListener {

    fun onClassificationSuccess(classification: Set<Classification>)

}