package com.livestyled.events.view.activity

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.view.Gravity
import android.view.Menu
import android.view.MenuItem
import android.widget.PopupMenu
import com.livestyled.R
import com.livestyled.events.repo.Classification
import com.livestyled.events.view.fragment.EventsFragment
import com.livestyled.events.view.fragment.EventsFragmentListener
import com.livestyled.util.TextUtils
import kotlinx.android.synthetic.main.activity_main.*


private const val EVENT_FRAGMENT_TAG = "EVENT_FRAGMENT"

class MainActivity : AppCompatActivity(), EventsFragmentListener {


    private var fragment: Fragment? = null
    private var classificationPopupMenu: PopupMenu? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        setSupportActionBar(mainActivityToolbar)


        supportActionBar?.title = TextUtils.getString(resId = R.string.app_name)

        if (savedInstanceState != null) {
            fragment = supportFragmentManager.getFragment(savedInstanceState, EVENT_FRAGMENT_TAG)

        } else {
            fragment = supportFragmentManager.findFragmentById(R.id.eventsFragment)

        }

    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_main_activity, menu)

        return true

    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            R.id.filterAction -> {

                classificationPopupMenu?.show()

            }
        }

        return true

    }

    override fun onSaveInstanceState(outState: Bundle?) {
        super.onSaveInstanceState(outState)

        if (fragment != null)
            supportFragmentManager.putFragment(outState!!, EVENT_FRAGMENT_TAG, fragment!!)

    }

    override fun onClassificationSuccess(classification: Set<Classification>) {

        classificationPopupMenu = PopupMenu(this, mainActivityToolbar, Gravity.END)
        classification.forEach {
            classificationPopupMenu?.menu?.add(it.name)
        }
        classificationPopupMenu?.setOnMenuItemClickListener {

            if (fragment is EventsFragment)
                (fragment as EventsFragment).filterBy(it.title.toString())
            return@setOnMenuItemClickListener true
        }
    }
}
