package com.livestyled.events.view.fragment


import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.livestyled.App
import com.livestyled.R
import com.livestyled.base.view.BaseFragment
import com.livestyled.events.EventsContract
import com.livestyled.events.view.WrapContentLinearLayoutManager
import com.livestyled.events.view.adapter.EventsAdapter
import com.livestyled.events.view.dto.EventListItem
import com.livestyled.events.view.dto.EventUIDTO
import kotlinx.android.synthetic.main.fragment_events.*


/**
 * A simple [BaseFragment] subclass.
 * Use the [EventsFragment.newInstance] factory method to
 * create an instance of this fragment.
 *
 */

private const val EVENTS_LIST_TAG = "EVENTSLIST"
private const val FILTER_BY_TAG = "FILTER_BY"
private const val FIRST_VISIBLE_CELL_TAG = "FIRST_VISIBLE_CELL"

class EventsFragment : BaseFragment<EventsContract.EventsView, EventsContract.EventsPresenter>(),
        EventsContract.EventsView, EventsAdapter.OnFavouriteClickListener<EventListItem> {


    private var viewAdapter: EventsAdapter? = null
    private lateinit var viewManager: WrapContentLinearLayoutManager
    private var eventsFragmentListener: EventsFragmentListener? = null
    private var mEvents: EventUIDTO? = null
    override fun inject() {

        App.applicationComponent?.inject(this@EventsFragment)

    }

    companion object {
        fun newInstance() = EventsFragment()
    }


    override fun onAttach(context: Context?) {
        super.onAttach(context)
        eventsFragmentListener = context as? EventsFragmentListener

    }

    override fun onDetach() {
        super.onDetach()
        eventsFragmentListener = null

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_events, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewManager = WrapContentLinearLayoutManager(context)
        swipeRefreshEvents.isEnabled = false

    }


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        if (savedInstanceState != null) {

            mEvents = savedInstanceState.getParcelable(EVENTS_LIST_TAG)
            val filterBy = savedInstanceState.getString(FILTER_BY_TAG, getString(R.string.all))
            val firstVisibleCellIndex = savedInstanceState.getInt(FIRST_VISIBLE_CELL_TAG, 0)
            showEventsSuccess(mEvents!!, filterBy, firstVisibleCellIndex)

        } else {
            presenter.getEvents()

        }

    }

    override fun showError(error: String) {
        loadingEventsTextView.text = error
        loadingEventsTextView.visibility = View.VISIBLE

    }

    override fun showEventsSuccess(events: EventUIDTO, mFilterBy: String,
                                   mFirstVisibleCellIndex: Int) {
        this.mEvents = events
        loadingEventsTextView.visibility = View.GONE
        eventsFragmentListener?.onClassificationSuccess(events.genreClassification)
        viewAdapter = EventsAdapter(onFavouriteClickListener = this@EventsFragment, filterBy = mFilterBy)
        eventsSearchRecyclerView.apply {
            layoutManager = viewManager
            adapter = viewAdapter
        }
        viewAdapter?.addItems(this.mEvents!!.eventItems)
        filterBy(mFilterBy)

        viewManager.scrollToPosition(mFirstVisibleCellIndex)


    }

    override fun onSaveInstanceState(outState: Bundle) {

        mEvents?.eventItems = viewAdapter?.getItems()!!

        outState.putParcelable(EVENTS_LIST_TAG, this.mEvents)
        outState.putString(FILTER_BY_TAG, viewAdapter?.filterBy)
        outState.putInt(FIRST_VISIBLE_CELL_TAG, viewManager.findFirstVisibleItemPosition())
        super.onSaveInstanceState(outState)

    }


    fun filterBy(by: String) = viewAdapter?.filter?.filter(by)


    override fun showLoading() {

        swipeRefreshEvents.isRefreshing = true
        loadingEventsTextView.text = getString(R.string.loading)
    }

    override fun hideLoading() {
        swipeRefreshEvents.isRefreshing = false
    }

    override fun onClick(event: EventListItem, position: Int) {

        viewAdapter?.updateItem(event)

    }
}
