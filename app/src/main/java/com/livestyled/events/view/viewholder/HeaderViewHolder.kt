package com.livestyled.events.view.viewholder

import android.support.v7.widget.RecyclerView
import android.view.View
import kotlinx.android.synthetic.main.event_header_item_view.view.*

/**
 * Created by Eslam Hussein on 7/10/18.
 */
class HeaderViewHolder(view: View) : RecyclerView.ViewHolder(view) {

    val headerTitleTextView = view.headerTitleTextView!!

}