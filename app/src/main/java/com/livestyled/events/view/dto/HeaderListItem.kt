package com.livestyled.events.view.dto

import kotlinx.android.parcel.Parcelize

/**
 * Created by Eslam Hussein on 7/13/18.
 */
@Parcelize
data class HeaderListItem(val headerName: String) : ListItem() {
    override val type: Int
        get() = ListItem.HEADER_VIEW
}


