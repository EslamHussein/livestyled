package com.livestyled.events.view.dto

import android.os.Parcelable
import com.livestyled.events.repo.Classification
import kotlinx.android.parcel.Parcelize

/**
 * Created by Eslam Hussein on 7/10/18.
 */
@Parcelize
data class EventUIDTO(var eventItems: List<ListItem>, val genreClassification: Set<Classification>) : Parcelable