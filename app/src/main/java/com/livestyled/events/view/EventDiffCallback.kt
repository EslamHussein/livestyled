package com.livestyled.events.view

import android.support.v7.util.DiffUtil
import com.livestyled.events.view.dto.ListItem


class EventDiffCallback(private val mOldEventList: List<ListItem>,
                        private val mNewEventList: List<ListItem>) : DiffUtil.Callback() {

    override fun getOldListSize(): Int = mOldEventList.size


    override fun getNewListSize(): Int = mNewEventList.size


    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return mOldEventList[oldItemPosition].type == mNewEventList[newItemPosition].type
    }

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        val oldItem = mOldEventList[oldItemPosition]
        val newEvent = mNewEventList[newItemPosition]

        return oldItem == newEvent
    }

}
