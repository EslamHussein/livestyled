package com.livestyled.events.view.dto

import com.livestyled.events.repo.Event
import kotlinx.android.parcel.Parcelize

/**
 * Created by Eslam Hussein on 7/13/18.
 */
@Parcelize
data class EventListItem(val event: Event) : ListItem() {
    override val type: Int
        get() = ListItem.EVENT_VIEW
}


