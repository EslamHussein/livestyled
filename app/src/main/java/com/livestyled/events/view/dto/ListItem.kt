package com.livestyled.events.view.dto

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

/**
 * Created by Eslam Hussein on 7/13/18.
 */

abstract class ListItem : Parcelable {

    abstract val type: Int

    companion object {

        const val HEADER_VIEW = 0
        const val EVENT_VIEW = 1
        const val UNKNOWN_VIEW = -1
    }
}