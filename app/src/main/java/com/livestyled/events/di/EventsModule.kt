package com.livestyled.events.di

import com.livestyled.base.cloud.AppServices
import com.livestyled.base.di.RxModule
import com.livestyled.events.EventsContract
import com.livestyled.events.business.EventsBusiness
import com.livestyled.events.business.EventsBusinessImpl
import com.livestyled.events.presenter.EventsPresenterImpl
import com.livestyled.events.repo.EventsCloudRepo
import com.livestyled.events.repo.EventsCloudRepoImpl
import dagger.Module
import dagger.Provides
import io.reactivex.Scheduler
import javax.inject.Named

/**
 * Created by Eslam Hussein on 7/7/18.
 */

@Module(includes = [(RxModule::class)])
class EventsModule {

    @Provides
    fun provideEventsCloudRepo(appServices: AppServices): EventsCloudRepo {

        return EventsCloudRepoImpl(appServices)
    }

    @Provides
    fun provideEventsPresenter(
            eventsBusiness: EventsBusiness, @Named("main_scheduler") mainScheduler: Scheduler,
            @Named("io_scheduler") subscribeOnScheduler: Scheduler): EventsContract.EventsPresenter {

        return EventsPresenterImpl(eventsBusiness, mainScheduler, subscribeOnScheduler)
    }

    @Provides
    fun provideEventsBusiness(cloudRepo: EventsCloudRepo): EventsBusiness {
        return EventsBusinessImpl(cloudRepo)

    }
}