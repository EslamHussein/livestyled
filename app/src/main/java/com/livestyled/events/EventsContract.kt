package com.livestyled.events

import com.cib.a7araka.base.view.MvpView
import com.livestyled.base.presenter.BasePresenter
import com.livestyled.events.view.dto.EventUIDTO

/**
 * Created by Eslam Hussein on 7/7/18.
 */
interface EventsContract {


    interface EventsView : MvpView {

        fun showError(error: String)
        fun showEventsSuccess(events: EventUIDTO, mFilterBy: String, mFirstVisibleCellIndex: Int = 0)
        fun showLoading()
        fun hideLoading()

    }

    abstract class EventsPresenter : BasePresenter<EventsView>() {
        abstract fun getEvents()
    }

}
