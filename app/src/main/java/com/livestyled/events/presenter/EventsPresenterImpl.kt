package com.livestyled.events.presenter

import com.livestyled.base.error.AppException
import com.livestyled.events.EventsContract
import com.livestyled.events.business.EventsBusiness
import io.reactivex.Scheduler
import io.reactivex.rxkotlin.subscribeBy
import javax.inject.Inject
import javax.inject.Named

/**
 * Created by Eslam Hussein on 7/7/18.
 */


class EventsPresenterImpl
@Inject constructor(var eventsBusiness: EventsBusiness,
                    @Named("main_scheduler") private var observeScheduler: Scheduler,
                    @Named("io_scheduler") var subscribeOnScheduler: Scheduler) : EventsContract.EventsPresenter() {

    override fun getEvents() {

        view?.showLoading()

        addDisposable(eventsBusiness.getEvents().observeOn(observeScheduler)
                .subscribeOn(subscribeOnScheduler).subscribeBy(

                        onNext = {
                            view?.showEventsSuccess(it, it.genreClassification.first().name)

                        },
                        onError = {
                            if (it is AppException)
                                view?.showError(it.errorMessage)
                            view?.hideLoading()

                        },
                        onComplete = {

                            view?.hideLoading()
                        }

                ))

    }
}