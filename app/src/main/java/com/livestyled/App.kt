package com.livestyled

import android.app.Application
import com.livestyled.base.di.ApplicationComponent
import com.livestyled.base.di.DaggerApplicationComponent
import com.livestyled.base.di.cloud.CloudModule
import com.livestyled.base.di.cloud.ServiceModule
import com.livestyled.events.di.EventsModule

/**
 * Created by Eslam Hussein on 6/11/18.
 */
class App : Application() {
    override fun onCreate() {
        super.onCreate()

        instance = this

        applicationComponent = DaggerApplicationComponent.builder().
                cloudModule(CloudModule()).
                eventsModule(EventsModule()).
                serviceModule(ServiceModule()).build()

    }


    companion object {


        private var instance: App? = null
        var applicationComponent: ApplicationComponent? = null

        fun get(): App {
            if (instance == null)
                throw IllegalStateException("Something went horribly wrong!!, no application attached!")
            return instance as App
        }
    }
}