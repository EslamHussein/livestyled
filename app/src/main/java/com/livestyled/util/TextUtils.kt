package com.livestyled.util


import android.content.Context
import android.support.annotation.StringRes
import com.livestyled.App


object TextUtils {

    private const val EMPTY_STRING_PATTERN = "^$|\\s+"

    fun getString(context: Context = App.get(), @StringRes resId: Int): String {
        return context.getString(resId)
    }


    fun isEmptyString(str: String?): Boolean {
        return str == null || str.isEmpty() ||
                str.matches(EMPTY_STRING_PATTERN.toRegex())
    }
}
