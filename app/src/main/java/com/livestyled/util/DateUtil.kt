package com.livestyled.util

import java.text.SimpleDateFormat
import java.util.*

/**
 * Created by Eslam Hussein on 7/10/18.
 */
object DateUtil {


    private const val UPC_DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss'Z'"
    fun getCurrentDateTime(): String = SimpleDateFormat(UPC_DATE_FORMAT, Locale.ENGLISH).format(Calendar.getInstance().time)


}