package com.livestyled.base.presenter


import com.cib.a7araka.base.view.MvpView

/**
 * Created by Eslam Hussein on 5/14/16.
 */
interface MvpPresenter<in V : MvpView> {

    /**
     * Called when an `MvpView` is attached to this presenter.
     *
     * @param view The attached `MvpView`
     */
    fun onAttach(view: V)

    /**
     * Called when the view is resumed according to Android components
     * NOTE: this method will only be called for presenters that override it.
     */
    fun onResume()

    /**
     * Called when an `MvpView` is detached from this presenter.
     */
    fun onDetach()

}
