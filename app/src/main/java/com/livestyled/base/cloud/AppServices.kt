package com.livestyled.base.cloud

import com.livestyled.events.repo.EventsResponse
import com.livestyled.util.DateUtil
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Query

/**
 * Created by Eslam Hussein on 7/7/18.
 */
interface AppServices {

    @GET(CloudConfig.EVENTS_URL)
    fun getEvents(@Query("city") city: String = "london",
                  @Query("startDateTime") startDate: String = DateUtil.getCurrentDateTime(),
                  @Query("page") page: Int? = 0,
                  @Query("size") size: Int? = 50): Observable<EventsResponse>
}