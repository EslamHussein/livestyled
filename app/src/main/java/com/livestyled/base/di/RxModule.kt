package com.livestyled.base.di

import dagger.Module
import dagger.Provides
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.Scheduler
import io.reactivex.schedulers.Schedulers
import javax.inject.Named


/**
 * Created by Eslam Hussein on 7/10/18.
 */
@Module
class RxModule {

    @Provides
    @Named("main_scheduler")
    fun provideMainScheduler(): Scheduler {
        return AndroidSchedulers.mainThread()
    }

    @Provides
    @Named("io_scheduler")
    fun provideIOScheduler(): Scheduler {
        return Schedulers.io()
    }
}