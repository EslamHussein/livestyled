package com.livestyled.base.di.cloud

import com.livestyled.base.cloud.AppServices
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit

/**
 * Created by Eslam Hussein on 7/7/18.
 */
@Module
class ServiceModule {

    @Provides
    fun provideFacebookService(retrofit: Retrofit): AppServices {
        return retrofit.create(AppServices::class.java)
    }

}