package com.livestyled.base.di.cloud

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.livestyled.BuildConfig
import com.livestyled.base.cloud.ApiKeyInterceptor
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

/**
 * Created by Eslam Hussein on 7/7/18.
 */

@Module
class CloudModule {

    @Provides
    fun provideOkHttpClint(httpInspector: HttpLoggingInterceptor,
                           apiKeyInterceptor: ApiKeyInterceptor): OkHttpClient {

        return OkHttpClient().newBuilder()
                .addInterceptor(httpInspector)
                .addInterceptor(apiKeyInterceptor)
                .build()

    }

    @Provides
    fun provideHttpLoggingInterceptor(): HttpLoggingInterceptor {
        val inspector = HttpLoggingInterceptor()
        inspector.level = HttpLoggingInterceptor.Level.BODY
        return inspector
    }


    @Provides
    fun provideGsonBuilder(): Gson {
        return GsonBuilder().create()
    }

    @Provides
    fun provideBaseUrl(): String {
        return BuildConfig.BASE_URL
    }

    @Provides
    fun provideRetrofit(baseUrl: String, gson: Gson, okHttpClient: OkHttpClient): Retrofit {

        return Retrofit.Builder()
                .baseUrl(baseUrl).client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build()
    }

    @Provides
    fun provideApiKeyInterceptor() = ApiKeyInterceptor()
}