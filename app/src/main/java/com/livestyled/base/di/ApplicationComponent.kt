package com.livestyled.base.di

import com.livestyled.base.di.cloud.CloudModule
import com.livestyled.base.di.cloud.ServiceModule
import com.livestyled.events.di.EventsModule
import com.livestyled.events.view.fragment.EventsFragment
import dagger.Component
import javax.inject.Singleton

/**
 * Created by Eslam Hussein on 7/7/18.
 */

@Singleton
@Component(modules = [CloudModule::class, EventsModule::class, ServiceModule::class, RxModule::class])
interface ApplicationComponent {
    fun inject(target: EventsFragment)

}