package com.livestyled.base.view


import android.content.Context
import android.support.v4.app.Fragment
import com.cib.a7araka.base.view.MvpView
import com.livestyled.base.presenter.MvpPresenter
import javax.inject.Inject


abstract class BaseFragment<V : MvpView, P : MvpPresenter<V>> : Fragment(), MvpView {


    @Inject
    open lateinit var presenter: P

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        inject()
        presenter.onAttach(view = this as V)
    }

    abstract fun inject()

    override fun onResume() {
        super.onResume()

        presenter.onResume()
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.onDetach()
    }


}
